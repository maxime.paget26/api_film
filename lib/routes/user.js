'use strict';

const Joi = require('joi');
const Jwt = require('@hapi/jwt');
const User = require('../models/user');
const Boom = require('@hapi/boom');

require('dotenv').config();

module.exports = [{
    method: 'post',
    path: '/user',
    options: {
        description: 'Create a use account',
        tags: ['api'],
        validate: {
            payload: Joi.object({
                firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
                lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
                username: Joi.string().required().example('JhonDoe04').description('The user pseudo'),
                password: Joi.string().min(8).required().description('User password'),
                mail: Joi.string().required().example('email@domain.fr').email().description('User email')
            })
        },
        auth: false
    },
    handler: async (request, h) => {

        const { userService } = request.services();

        return await userService.create(request.payload);
    }
},  {
    method: 'GET',
    path: '/user',
    options: {
        description: 'Fetch all users',
        tags: ['api'],
        auth: {
            scope: ['admin', 'user']
        }
    },
    handler: async (request, h) => {

        const { userService } = request.services();

        return await userService.select(request.payload);
    }
}, {
    method: 'GET',
    path: '/user/{id}',
    options: {
        description: 'Fetch the details of a user, a user with low privilege can only see his account detail, an admin can see the detail of all users',
        validate: {
            params: Joi.object({
                id: User.field('id').tailor('full')
            })
        },
        tags: ['api'],
        auth: {
            scope: ['admin', 'user']
        }
    },
    handler: async (request, h) => {

        if (request.auth.credentials.scope !== 'admin' && request.params.id !== request.auth.credentials.id) {
            throw new Boom.forbidden('You can see only your account detail, please enter your account id');
        }

        const { userService } = request.services();
        return await userService.show(request.params.id);
    }
},  {
    method: 'DELETE',
    path: '/user/{id}',
    options: {
        description: 'Delete a user by id',
        tags: ['api'],
        validate: {
            params: Joi.object({
                id: Joi.number().required().example(1).description('id of the user')
            })
        },
        auth: {
            scope: ['admin']
        }
    },
    handler: async (request, h) => {

        const { userService } = request.services();
        await userService.delete(request.params);
        return '';
    }
},
{
    method: 'PATCH',
    path: '/user/{id}',
    options: {
        tags: ['api'],
        description: 'Update a user',
        validate: {
            params: Joi.object({
                id: Joi.number().required().example(1).description('id of the user')
            }),
            payload: Joi.object({
                firstName: User.field('firstName').tailor('patch'),
                lastName: User.field('lastName').tailor('patch'),
                username: User.field('username').tailor('patch'),
                password: User.field('password').tailor('patch'),
                mail: User.field('mail').tailor('patch'),
                role: User.field('role')
            })
        },
        auth: {
            scope: ['admin']
        }
    },
    handler: async (request, h) => {

        const { userService } = request.services();
        await userService.patch(request.params.id, request.payload);
        return '';
    }
}, {
    method: 'POST',
    path: '/user/login',
    options: {
        description: 'Connect to a user account',
        tags: ['api'],
        validate: {
            payload: Joi.object({
                identifiant: Joi.string().required().min(3).example('John').description('Firstname of the user'),
                password: Joi.string().required().description('User password')
            })
        },
        auth: false
    },
    handler: async (request, h) => {

        const { userService } = request.services();

        const user = await userService.login(request.payload);
        if (!user) {
            return h.response('Invalid Credential').code(401);
        }


        return { login: Jwt.token.generate(
            {
                aud: 'urn:audience:iut',
                iss: 'urn:issuer:iut',
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.mail,
                id: user.id,
                scope: user.role
            },
            {
                key: process.env.JWT_KEY, // La clé qui est définit dans lib/auth/strategies/jwt.js
                algorithm: 'HS512'
            },
            {
                ttlSec: 14400 // 4 hours
            })
        };
    }
}, {
    method: 'post',
    path: '/user/addFavorite/{id}',
    options: {
        description: 'Add a film to the favorite list of the current user connect',
        tags: ['api'],
        validate: {
            params: Joi.object({
                id: Joi.number().required().example(1).description('film id')
            })
        },
        auth: {
            scope: ['user', 'admin']
        }
    },
    handler: async (request, h) => {

        const { userService } = request.services();
        await userService.addFavorite(request.auth.credentials.id, request.params.id);
        return '';
    }
},  {
    method: 'post',
    path: '/user/rmFavorite/{id}',
    options: {
        description: 'Remove a film from the favorite list of the current user connect',
        tags: ['api'],
        validate: {
            params: Joi.object({
                id: Joi.number().required().example(1).description('id of the film')
            })
        },
        auth: {
            scope: ['user', 'admin']
        }
    },
    handler: async (request, h) => {

        const { userService } = request.services();
        await userService.rmFavorite(request.auth.credentials.id, request.params.id);
        return '';
    }
}];

