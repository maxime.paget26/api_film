'use strict';

const Boom = require('@hapi/boom');
const { Service } = require('@hapipal/schmervice');
const EncryptIut = require('tppackage');
const MailerHelper = require('../helpers/mailer');

require('dotenv').config();

module.exports = class UserService extends Service {
    /**
     * @type EncryptIut
     */
    #crypto;
    initialize() {

        const { User } = this.server.models();
        this.userManager = User;
        this.#crypto = new EncryptIut(process.env.ENCRYPT_KEY);
    }

    async create(user) {

        const [potentiallyUser] = await this.userManager.query().select().where('username', user.username).orWhere('mail', user.mail);

        if (potentiallyUser) {
            throw Boom.badRequest('This identifiant is already used');
        }

        user.password = this.#crypto.encrypt(user.password);
        const userCreated = await this.userManager.query().insertAndFetch(user);
        MailerHelper.instance.send(user.mail, 'Inscription réussi', `📺 Bienvenue cher cynophile ! \n Merci ${user.firstName} pour votre abonnement à notre platforme 📺`);
        return userCreated;
    }

    async select() {

        return await this.userManager.query().select('id', 'firstname', 'lastname', 'username', 'mail');
    }

    async show(id) {

        const [user] = await this.userManager.query()
            .select()
            .where('id', id);

        if (!user) {
            throw new Boom.badRequest('Unknow this user');
        }

        user.favorites = await this.userManager.query()
            .select('movies.id', 'movies.title', 'movies.author', 'movies.description', 'movies.releaseDate')
            .joinRelated('movies')
            .where('user_id', id);

        return user;
    }

    async delete({ id }) {

        const userToDelete = await this.userManager.query().findById(id);

        if (!userToDelete) {
            throw new Boom.badRequest('Unknow user id');
        }

        const favorites = await this.getFavorites(id);

        for (const favorite of favorites) {
            await this.userManager.relatedQuery('movies').for(id).unrelate().where('movie_id', favorite.id);
        }

        await this.userManager.query().deleteById(id);
        return '';
    }

    async patch(id, payload) {

        const userToUpdate = await this.userManager.query().findById(id);
        if (!userToUpdate) {
            throw new Boom.badRequest('Unknow user id');
        }

        if (payload.password) {
            payload.password = this.#crypto.encrypt(payload.password);
        }

        return await this.userManager.query().findById(id).patch(payload || {});
    }

    async login({ identifiant, password }) {

        const [user] = await this.userManager.query().select().where('username', identifiant).orWhere('mail', identifiant);
        if (user && this.#crypto.equals(password, user.password)) {
            return user;
        }
    }

    async getFavorites(userId) {

        return await this.userManager.relatedQuery('movies').for(userId);
    }

    async addFavorite(userId, filmId) {

        const { Film } = this.server.models();
        const favoriteMovie = await Film.query().findById(filmId);
        if (!favoriteMovie) {
            throw new Boom.badRequest('Unknow this film');
        }

        const favorites = await this.getFavorites(userId);

        if (favorites.map((film) => film.id).includes(filmId)) {
            throw Boom.badRequest('The user already has the movie in his favorite list');
        }

        await this.userManager.relatedQuery('movies').for(userId).relate(filmId);
    }

    async rmFavorite(userId, filmId) {

        const favorites = await this.getFavorites(userId);

        if (!favorites.map((film) => film.id).includes(filmId)) {
            throw Boom.badRequest('Unknow this film in the favorite list of the user');
        }

        await this.userManager.relatedQuery('movies').for(userId).unrelate().where('movie_id', filmId);
    }
};

